# What is this project about?

This a simple Rest API that allows you to request the Weather of a city. The Weather is randomly generated. However, the API return the same weather for the same city on the same date.

E.g. you request the weather of London on 2021-01-01, the API will return the same weather for London when you request the weather of London on 2021-01-01 again.

# How to run the project?

1. Clone the project
2. Run the following command to start the server:
```bash
make {your System} # e.g. make linux
cd build/{your System} # e.g. cd build/linux
./weather-api
```
3. The server will start on port 3000. You can access the API on http://localhost:3000/swagger/index.html

## Run with Docker

1. Clone the project
2. Run the following command to start the server:
```bash
make docker_immage
docker run -p 3000:3000 codeberg.org/birkenfunk/sqs-external-api:latest
```
3. The server will start on port 3000. You can access the API on http://localhost:3000/swagger/index.html

# Testing if the API is working

You can test if the API is working by running the following command:
```bash
curl http://localhost:3000/weather/test
```
This command will return the following response:
```json
{
  "location":"Test",
  "temperature":"20°C",
  "humidity":"50%",
  "sunHours":5,
  "windSpeed":"10km/h",
  "weather":"Sunny",
  "date":"2024-01-01"
}
```
This response will be the same every time you run the command.

# How to request the weather of a city?

You can request the weather of a city by running the following command:
```bash
curl http://localhost:3000/weather/{city}
```
Replace {city} with the name of the city you want to request the weather from.

E.g. to request the weather of London, you can run the following command:
```bash
curl http://localhost:3000/weather/London
```
This command will return the following response:
```json
{
  "location":"London",
  "temperature":"20°C",
  "humidity":"50%",
  "sunHours":5,
  "windSpeed":"10km/h",
  "weather":"Sunny",
  "date":"2024-01-01"
}
```
The values of the response will be randomly generated. However, the response will be the same every time you request the weather of London on 2024-01-01.
The date will be the current date when you request the weather and the City will be the city you requested.