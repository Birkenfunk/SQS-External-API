package routes

import (
	_ "codeberg.org/Birkenfunk/SQS-External-API/docs"
	"codeberg.org/Birkenfunk/SQS-External-API/handlers"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/swaggo/http-swagger"
	"net/http"
)

func InitRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Mount("/api/v1", apiRoutes())
	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:3000/swagger/doc.json"), //The url pointing to API definition
	))

	return r
}

func apiRoutes() http.Handler {
	r := chi.NewRouter()
	r.Get("/health", handlers.Health)
	r.Get("/weather/{location}", handlers.GetWeather)

	return r
}
