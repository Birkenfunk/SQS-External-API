package handlers

import (
	"codeberg.org/Birkenfunk/SQS-External-API/dto"
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"hash/fnv"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

// GetWeather returns the weather for a given location
//
//	@Summary		Get the weather for a given location
//	@Description	Get the weather for a given location
//	@Tags			weather
//	@Produce		json
//	@Param			location	path		string	true	"The location to get the weather for"
//	@Success		200	{object}	dto.WeatherDto	"OK"
//	@Failure		400			{object}	string	"The location is missing"
//	@Failure		500			{object}	string	"An error occurred"
//	@Router			/weather/{location} [get]
func GetWeather(rw http.ResponseWriter, r *http.Request) {
	location := chi.URLParam(r, "location")
	if location == "" {
		_, err := rw.Write([]byte("The location is missing"))
		if err != nil {
			return
		}
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	if location == "test" {
		weather := dto.WeatherDto{
			Location:    "Test",
			Temperature: "20°C",
			Humidity:    "50%",
			SunHours:    5,
			WindSpeed:   "10km/h",
			Weather:     "Sunny",
			Date:        time.Date(2024, 1, 1, 1, 1, 1, 1, time.Local).Format("2006-01-02"),
		}
		response, err := json.Marshal(weather)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		_, err = rw.Write(response)
		if err != nil {
			return
		}
		return
	}
	weather := generateWeatherFromLocation(location)
	response, err := json.Marshal(weather)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, err = rw.Write(response)
	if err != nil {
		return
	}
}

func generateWeatherFromLocation(location string) dto.WeatherDto {
	today := time.Now().Format("2006-01-02")
	hash := hash(location + today)
	randomGenerator := rand.New(rand.NewSource(int64(hash)))

	temp := 50 - randomGenerator.Intn(100)
	tempAsString := strconv.Itoa(temp)
	humidity := randomGenerator.Intn(100)
	humidityAsString := strconv.Itoa(humidity)
	sunHours := randomGenerator.Intn(10)
	windSpeed := randomGenerator.Intn(50)
	windSpeedAsString := strconv.Itoa(windSpeed)
	weatherState := randomGenerator.Intn(3)
	var weather string
	switch weatherState {
	case 0:
		weather = "Sunny"
	case 1:
		weather = "Cloudy"
	case 2:
		weather = "Rainy"
	default:
		weather = "Sunny"
	}
	return dto.WeatherDto{
		Location:    location,
		Temperature: tempAsString + "°C",
		Humidity:    humidityAsString + "%",
		SunHours:    sunHours,
		WindSpeed:   windSpeedAsString + "km/h",
		Weather:     weather,
		Date:        time.Now().Format("2006-01-02"),
	}
}

func hash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}
