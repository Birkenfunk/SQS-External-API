package main

import (
	"codeberg.org/Birkenfunk/SQS-External-API/routes"
	"net/http"
)

func init() {
	println("Starting the Server...")
}

// @title			SQS External API
// @description	This is the external API for the SQS project
// @version		1
// @host			localhost:3000
// @BasePath		/api/v1
func main() {
	r := routes.InitRouter()

	err := http.ListenAndServe(":3000", r)
	if err != nil {
		panic(err)
	}
}
