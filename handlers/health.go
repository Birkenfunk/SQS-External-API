package handlers

import "net/http"

// Health returns a 200 OK
//	@Summary		Health check
//	@Description	Health check
//	@Tags			health
//	@Produce		json
//	@Success		200	{object}	string	"OK"
//	@Router			/health [get]
func Health(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusOK)
}
